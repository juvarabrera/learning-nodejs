# learning-nodejs

## Background

We have been using node.js just to install javascript libraries and using them on our web application. I have not used it formally as a backend although I have experienced creating an API using Express.js. This repository will just document what I am learning from an online course, codecademy.

## Where I am learning
I am learning Node.js on Codecademy (https://www.codecademy.com/courses/learn-node-js/)

## Resources
_WIP_
